#!/bin/sh

MAKE='make -j2'
CDS="cd $LFS/sources"
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin

prolog () {
	printf "\n$1\n"
	$CDS
	tar -xf $1.tar.*
	cd $1
}

end () {
	$CDS
	rm -rf $1
}
