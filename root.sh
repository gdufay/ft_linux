#!/bin/sh

echo "Installing host requirements"
apt install bash binutils bison bzip2 coreutils\
	diffutils findutils gawk gcc g++ grep gzip\
	m4 make patch perl python sed tar texinfo\
	xz-utils wget
sudo ln -sf bash /bin/sh

echo "Creating new partition"
cat << EOF | fdisk /dev/sda
n
p
3

+30G
n
p
4

+4G
t
4
82
w
q
EOF
fdisk -l

echo "Creating filesystem on the part"
mkfs -v -t ext4 $PARTITION
mkswap $SWAP

echo "Mounting new partition"
mkdir -pv $LFS
mount -v -t ext4 $PARTITION $LFS
/sbin/swapon -v $SWAP
echo "permissions are:" && mount | grep $PARTITION

echo "Download packages"
mkdir -v $LFS/sources
chmod -v a+wt $LFS/sources #set sticky bit
wget "http://www.linuxfromscratch.org/lfs/view/stable/wget-list"
wget --input-file=wget-list --continue --directory-prefix=$LFS/sources

echo "Creating the $LFS/tools Directory"
mkdir -v $LFS/tools
ln -sv $LFS/tools /

echo "Adding the LFS User"
groupadd lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs
passwd lfs
chown -v lfs $LFS/tools
chown -v lfs $LFS/sources
