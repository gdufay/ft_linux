#!/bin/sh

source $REPO_PATH/utils.sh

TMP_SYS_DIR=$REPO_PATH/tmp_system

echo "Setting Up the Environment"
umask 022
export LC_ALL LFS_TGT PATH

tmp_sys=(binutils-v1.sh gcc-v1.sh linux.sh glibc.sh libstdc++.sh binutils-v2.sh gcc-v2.sh tcl.sh expect.sh dejagnu.sh m4.sh ncurses.sh bash.sh bison.sh bzip.sh coreutils.sh diffutils.sh file.sh findutils.sh gawk.sh gettext.sh grep.sh gzip.sh make.sh patch.sh perl.sh python.sh sed.sh tar.sh texinfo.sh xz.sh)

for i in "${tmp_sys[@]}";
do
	sh $TMP_SYS_DIR/$i
done

echo "Stripping"
strip --strip-debug /tools/lib/*
/usr/bin/strip --strip-unneeded /tools/{,s}bin/*
rm -rf /tools/{,share}/{info,man,doc}
find /tools/{lib,libexec} -name \*.la -delete
