#!/bin/sh

LFS=/mnt/lfs
PARTITION="/dev/sda3"
SWAP="/dev/sda4"

env -i LFS=$LFS PARTITION=$PARTITION SWAP=$SWAP sudo -E sh root.sh
env -i LFS=$LFS REPO_PATH=$PWD su lfs -c "sh lfs.sh"

chown -R root:root $LFS/tools
