#!/bin/sh

source $REPO_PATH/utils.sh

NAME=tcl8.6.9

prolog $NAME-src
cd $NAME
cd unix
./configure --prefix=/tools
$MAKE
$MAKE install
chmod -v u+w /tools/lib/libtcl8.6.so
$MAKE install-private-headers
ln -sv tclsh8.6 /tools/bin/tclsh
end $NAME
