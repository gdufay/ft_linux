#!/bin/sh

source $REPO_PATH/utils.sh

NAME=bash-5.0

prolog $NAME
./configure --prefix=/tools --without-bash-malloc
$MAKE
$MAKE install
ln -sv bash /tools/bin/sh
end $NAME
