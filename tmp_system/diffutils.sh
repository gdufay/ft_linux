#!/bin/sh

source $REPO_PATH/utils.sh

NAME=diffutils-3.7

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME

