#!/bin/sh

source $REPO_PATH/utils.sh

NAME=grep-3.3

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
