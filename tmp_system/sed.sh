#!/bin/sh

source $REPO_PATH/utils.sh

NAME=sed-4.7

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
