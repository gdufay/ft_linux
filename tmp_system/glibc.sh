#!/bin/sh

source $REPO_PATH/utils.sh

NAME=glibc-2.30

prolog $NAME
mkdir -v build
cd       build
../configure                             \
      --prefix=/tools                    \
      --host=$LFS_TGT                    \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2                \
      --with-headers=/tools/include
# parallel make can fail so basic make
make
make install
case $(uname -m) in
  x86_64) ln -s /tools/lib/crt*.o /tools/lib/gcc/x86_64-lfs-linux-gnu/9.2.0/ ;;
esac
end $NAME
