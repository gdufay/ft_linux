#!/bin/sh

source $REPO_PATH/utils.sh

NAME=expect5.45.4

prolog $NAME
cp -v configure configure.orig
sed 's:/usr/local/bin:/bin:' configure.orig > configure
./configure --prefix=/tools       \
            --with-tcl=/tools/lib \
            --with-tclinclude=/tools/include
$MAKE
$MAKE SCRIPTS="" install
end $NAME
