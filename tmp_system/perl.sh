#!/bin/sh

source $REPO_PATH/utils.sh

NAME=perl-5.30.0

prolog $NAME
sh Configure -des -Dprefix=/tools -Dlibs=-lm -Uloclibpth -Ulocincpth
$MAKE
cp -v perl cpan/podlators/scripts/pod2man /tools/bin
mkdir -pv /tools/lib/perl5/5.30.0
cp -Rv lib/* /tools/lib/perl5/5.30.0
end $NAME
