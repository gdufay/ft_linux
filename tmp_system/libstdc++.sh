#!/bin/sh

source $REPO_PATH/utils.sh

NAME=gcc-9.2.0

prolog $NAME
mkdir -v build
cd       build
printf "\nlibstdc++\n"
../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --prefix=/tools                 \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-threads     \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/9.2.0
$MAKE
$MAKE install
end $NAME
