#!/bin/sh

source $REPO_PATH/utils.sh

NAME=make-4.2.1

prolog $NAME
sed -i '211,217 d; 219,229 d; 232 d' glob/glob.c
./configure --prefix=/tools --without-guile
$MAKE
$MAKE install
end $NAME
