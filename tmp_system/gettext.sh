#!/bin/sh

source $REPO_PATH/utils.sh

NAME=gettext-0.20.1

prolog $NAME
./configure --disabled-shared
$MAKE
cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /tools/bin
end $NAME
