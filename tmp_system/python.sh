#!/bin/sh

source $REPO_PATH/utils.sh

NAME=Python-3.7.4

prolog $NAME
sed -i '/def add_multiarch_paths/a \        return' setup.py
./configure --prefix=/tools --without-ensurepip
$MAKE
$MAKE install
end $NAME
