#!/bin/sh

source $REPO_PATH/utils.sh

NAME=ncurses-6.1

prolog $NAME
sed -i s/mawk// configure
./configure --prefix=/tools \
            --with-shared   \
            --without-debug \
            --without-ada   \
            --enable-widec  \
            --enable-overwrite
$MAKE
$MAKE install
ln -s libncursesw.so /tools/lib/libncurses.so
end $NAME
