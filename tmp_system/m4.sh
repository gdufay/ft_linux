#!/bin/sh

source $REPO_PATH/utils.sh

NAME=m4-1.4.18

prolog $NAME
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
