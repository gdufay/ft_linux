#!/bin/sh

source $REPO_PATH/utils.sh

NAME=dejagnu-1.6.2

prolog $NAME
./configure --prefix=/tools
$MAKE install
make check
end $NAME
