#!/bin/sh

source $REPO_PATH/utils.sh

NAME=findutils-4.6.0

prolog $NAME
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' gl/lib/*.c
sed -i '/unistd/a #include <sys/sysmacros.h>' gl/lib/mountlist.c
echo "#define _IO_IN_BACKUP 0x100" >> gl/lib/stdio-impl.h
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME



