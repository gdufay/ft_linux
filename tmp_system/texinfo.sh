#!/bin/sh

source $REPO_PATH/utils.sh

NAME=texinfo-6.6

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
