#!/bin/sh

source $REPO_PATH/utils.sh

NAME=bzip2-1.0.8

prolog $NAME
$MAKE
$MAKE PREFIX=/tools install
end $NAME
