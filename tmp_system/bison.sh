#!/bin/sh

source $REPO_PATH/utils.sh

NAME=bison-3.4.1

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
