#!/bin/sh

source $REPO_PATH/utils.sh

NAME=xz-5.2.4

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
