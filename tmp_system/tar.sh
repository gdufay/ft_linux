#!/bin/sh

source $REPO_PATH/utils.sh

NAME=tar-1.32

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
