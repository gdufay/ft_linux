#!/bin/sh

source $REPO_PATH/utils.sh

NAME=linux-5.2.8

prolog $NAME
$MAKE mrproper
$MAKE INSTALL_HDR_PATH=dest headers_install
cp -rv dest/include/* /tools/include
end $NAME
