#!/bin/sh

source $REPO_PATH/utils.sh

NAME=binutils-2.32

prolog $NAME
mkdir build && cd build
../configure --prefix=/tools --with-sysroot=$LFS --with-lib-path=/tools/lib \
	--target=$LFS_TGT --disable-nls --disable-werror && $MAKE
case $(uname -m) in
  x86_64) mkdir /tools/lib && ln -sv lib /tools/lib64 ;;
esac
$MAKE install
end $NAME
