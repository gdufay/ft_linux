#!/bin/sh

source $REPO_PATH/utils.sh

NAME=coreutils-8.31

prolog $NAME
./configure --prefix=/tools --enable-install-program=hostname
$MAKE
$MAKE install
end $NAME
