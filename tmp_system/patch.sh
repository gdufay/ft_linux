#!/bin/sh

source $REPO_PATH/utils.sh

NAME=patch-2.7.6

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
