#!/bin/sh

source $REPO_PATH/utils.sh

NAME=gzip-1.10

prolog $NAME
./configure --prefix=/tools
$MAKE
$MAKE install
end $NAME
